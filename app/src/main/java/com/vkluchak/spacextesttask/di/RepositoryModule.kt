package com.vkluchak.spacextesttask.di

import com.vkluchak.spacextesttask.api.LaunchApiService
import com.vkluchak.spacextesttask.repository.LaunchRepository
import com.vkluchak.spacextesttask.repository.LaunchRepositoryImpl
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    internal fun provideLaunchRepository(retrofit: Retrofit): LaunchRepository {
        return LaunchRepositoryImpl(retrofit.create(LaunchApiService::class.java))
    }
}