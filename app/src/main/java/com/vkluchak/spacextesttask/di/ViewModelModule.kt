/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vkluchak.spacextesttask.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.vkluchak.spacextesttask.viewmodel.LaunchViewModelFactory
import com.vkluchak.spacextesttask.ui.main.tabs.chart.ChartViewModel
import com.vkluchak.spacextesttask.ui.details.DetailsViewModel
import com.vkluchak.spacextesttask.ui.main.tabs.list.LaunchListViewModel

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ChartViewModel::class)
    abstract fun bindCreateViewModel(createTaskViewModel: ChartViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LaunchListViewModel::class)
    abstract fun bindListViewModel(launchListViewModel: LaunchListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailsViewModel::class)
    abstract fun bindDetailsViewModel(detailsViewModel: DetailsViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: LaunchViewModelFactory): ViewModelProvider.Factory
}
