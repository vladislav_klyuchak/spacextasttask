package com.vkluchak.spacextesttask.api

import io.reactivex.Single
import retrofit2.http.*


/**
 * REST API access points
 */
interface LaunchApiService {

    @GET("launches")
    fun getAllLaunches(): Single<List<LaunchResponse>>

    @GET("launches")
    fun getPagedLaunches(@Query("offset") startPosition: Int, @Query("limit") loadSize: Int): Single<List<LaunchResponse>>

    @GET("launches/{flight_number}")
    fun getLaunch(@Path("flight_number") flightNumber: Int): Single<LaunchResponse>

}

data class LaunchResponse(
    val details: String,
    val flight_number: Int,
    val is_tentative: Boolean,
    val launch_date_local: String,
    val launch_date_unix: Long,
    val launch_date_utc: String,
    val launch_failure_details: LaunchFailureDetails,
    val launch_site: LaunchSite,
    val launch_success: Boolean,
    val launch_window: Int,
    val launch_year: String,
    val mission_id: List<Any>,
    val links: Links,
    val mission_name: String,
    val rocket: Rocket,
    val ships: List<Any>,
    val static_fire_date_unix: Int,
    val static_fire_date_utc: String,
    val tbd: Boolean,
    val telemetry: Telemetry,
    val tentative_max_precision: String,
    val timeline: Timeline,
    val upcoming: Boolean
)

data class LaunchSite(
    val site_id: String,
    val site_name: String,
    val site_name_long: String
)

data class Rocket(
    val rocket_id: String,
    val rocket_name: String,
    val rocket_type: String
)

data class LaunchFailureDetails(
    val altitude: Any,
    val reason: String,
    val time: Int
)

data class Telemetry(
    val flight_club: Any
)

data class Timeline(
    val webcast_liftoff: Int
)

data class Links(
    val article_link: String,
    val flickr_images: List<String>,
    val mission_patch: String,
    val mission_patch_small: String,
    val presskit: String,
    val reddit_campaign: Any,
    val reddit_launch: String,
    val reddit_media: Any,
    val reddit_recovery: Any,
    val video_link: String,
    val wikipedia: String,
    val youtube_id: String
)
