package com.vkluchak.spacextesttask.ui.main.tabs.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxrelay2.ReplayRelay
import com.squareup.picasso.Picasso
import com.vkluchak.spacextesttask.R
import com.vkluchak.spacextesttask.api.LaunchResponse
import com.vkluchak.spacextesttask.databinding.ItemLaunchBinding
import com.vkluchak.spacextesttask.vo.getDate

class LaunchListAdapter(private val retryCallback: () -> Unit) : PagedListAdapter<LaunchResponse, LaunchViewHolder>(TaskDiffCallback) {

    val onClickSubject = ReplayRelay.create<LaunchResponse>()!!

    fun onClickSubjectRelay() = this.onClickSubject

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LaunchViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: ItemLaunchBinding =
            DataBindingUtil.inflate(inflater, R.layout.item_launch, parent, false)
        return LaunchViewHolder(binding)

    }

    override fun onBindViewHolder(holder: LaunchViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            onClickSubject.accept(getItem(position))
        }
        when (getItemViewType(position)) {
            R.layout.item_launch -> holder.bindTo(getItem(position)!!)
        }
    }

    override fun getItemViewType(position: Int): Int {
       return R.layout.item_launch
    }

    override fun getItemCount(): Int {
        return super.getItemCount()
    }

    companion object {
        val TaskDiffCallback = object : DiffUtil.ItemCallback<LaunchResponse>() {
            override fun areItemsTheSame(oldItem: LaunchResponse, newItem: LaunchResponse): Boolean {
                return oldItem.flight_number == newItem.flight_number
            }

            override fun areContentsTheSame(oldItem: LaunchResponse, newItem: LaunchResponse): Boolean {
                return oldItem == newItem
            }
        }
    }
}

class LaunchViewHolder(val binding: ItemLaunchBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bindTo(launch: LaunchResponse) {
        binding.launch = launch
        binding.tvDate.text = getDate(launch.launch_date_unix)

        Picasso.get()
            .load(launch.links.mission_patch_small)
            .into(binding.ivImage)


    }
}