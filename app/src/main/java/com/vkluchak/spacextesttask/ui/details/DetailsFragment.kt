package com.vkluchak.spacextesttask.ui.details

import android.Manifest.permission
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import com.tbruyelle.rxpermissions2.RxPermissions
import com.vkluchak.spacextesttask.databinding.FragmentDetailsBinding
import com.vkluchak.spacextesttask.di.Injectable
import com.vkluchak.spacextesttask.service.DownloadService
import com.vkluchak.spacextesttask.vo.getDate
import com.vkluchak.spacextesttask.vo.showToast
import io.reactivex.android.schedulers.AndroidSchedulers
import java.io.File
import java.io.FileOutputStream
import javax.inject.Inject

internal const val ARG_LAUNCH_ID = "arg_launch_id"

class DetailsFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentDetailsBinding
    private lateinit var model: DetailsViewModel
    private var launchId: Int = 0

    private var viewAdapter = LaunchImagesAdapter(emptyList<String>().toMutableList())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        arguments?.let {
            launchId = it.getInt(ARG_LAUNCH_ID)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        model = ViewModelProviders.of(this, viewModelFactory)
            .get(DetailsViewModel::class.java)

        subscribeToData()

        if(model.isFirstInit) {
            model.getLaunchDetails(launchId)
            model.isFirstInit = false
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentDetailsBinding.inflate(inflater, container, false)
        initAdapter()
        return binding.root
    }

    private fun subscribeToData(){
        model.errorReplayRelay
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                context?.showToast("Error \n $it")
            }.subscribe()

        model.launchDetailsRelay
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                binding.launch = it

                binding.tvLaunchTime.text = getDate(it.launch_date_unix)

                Picasso.get()
                    .load(it.links.mission_patch_small)
                    .into(binding.ivLaunchImage)

                val imageLinks = emptyList<String>().toMutableList()

                imageLinks.add(it.links.mission_patch)
                imageLinks.addAll(it.links.flickr_images)

                viewAdapter.setData(imageLinks)

            }.subscribe()
    }

    private fun initAdapter() {
        val linearLayoutManager = object : LinearLayoutManager(context, RecyclerView.VERTICAL, false) {
            override fun canScrollVertically() = false
        }

        binding.rvImageList.adapter = viewAdapter
        binding.rvImageList.layoutManager = linearLayoutManager

        viewAdapter.getPositionClicks()
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext { imageUrl ->
                if (!imageUrl.isNullOrBlank()) {
                    RxPermissions(activity!!)
                        .request(permission.WRITE_EXTERNAL_STORAGE)
                        .subscribe { granted ->
                            if (granted) {
                                activity!!.startService(
                                    DownloadService.getDownloadService(
                                        context!!, imageUrl,
                                        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).canonicalPath
                                    ))
                            } else {
                                // TODO permission
                                // Oups permission denied
                            }
                        }
                }
            }.subscribe()
    }


}