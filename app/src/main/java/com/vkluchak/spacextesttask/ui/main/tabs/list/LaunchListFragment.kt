package com.vkluchak.spacextesttask.ui.main.tabs.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vkluchak.spacextesttask.R
import com.vkluchak.spacextesttask.api.LaunchResponse
import com.vkluchak.spacextesttask.databinding.FragmentListBinding
import com.vkluchak.spacextesttask.di.Injectable
import com.vkluchak.spacextesttask.ui.details.ARG_LAUNCH_ID
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject


class LaunchListFragment : Fragment(), Injectable {

    private lateinit var launchAdapter: LaunchListAdapter
    private lateinit var model: LaunchListViewModel
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentListBinding

    private lateinit var navController: NavController


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        model = ViewModelProviders.of(this, viewModelFactory)
            .get(LaunchListViewModel::class.java)

        subscribeToData()

        if(model.isFirstInit) {
            model.launchList.observe(this, Observer<PagedList<LaunchResponse>> { launchAdapter.submitList(it) })
            model.isFirstInit = false
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentListBinding.inflate(inflater, container, false)
        initAdapter()
        binding.sRefreshLayout.setOnRefreshListener {
            requestData()
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)
    }

    private fun subscribeToData() {

        model.launchList.observe(this, Observer<PagedList<LaunchResponse>> {
            launchAdapter.submitList(it)
            binding.sRefreshLayout.isRefreshing = false
        })

        launchAdapter.onClickSubjectRelay()
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                val bundle = bundleOf(ARG_LAUNCH_ID to it.flight_number)
                navController.navigate(R.id.action_main_to_details, bundle)
            }.subscribe()

    }

    private fun initAdapter() {
        val linearLayoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        launchAdapter = LaunchListAdapter {
            model.retry()
        }
        binding.rvList.layoutManager = linearLayoutManager
        binding.rvList.adapter = launchAdapter
    }

    private fun requestData() {
        model.refresh()
    }
}