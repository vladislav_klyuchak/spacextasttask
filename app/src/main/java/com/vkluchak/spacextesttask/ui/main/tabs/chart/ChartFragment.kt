package com.vkluchak.spacextesttask.ui.main.tabs.chart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.vkluchak.spacextesttask.databinding.FargmentChartBinding
import com.vkluchak.spacextesttask.di.Injectable
import com.vkluchak.spacextesttask.vo.chart.custom.line.LineChartAdapter
import com.vkluchak.spacextesttask.vo.chart.custom.line.formatter.DefaultYAxisValueFormatter
import com.vkluchak.spacextesttask.vo.showToast
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class ChartFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FargmentChartBinding

    private lateinit var model: ChartViewModel

    private val defaultFormatter = DefaultYAxisValueFormatter()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        model = ViewModelProviders.of(this, viewModelFactory)
            .get(ChartViewModel::class.java)

        subscribeToData()

        if(model.isFirstInit) {
            model.getNumLaunchesForChart()
            model.isFirstInit = false
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FargmentChartBinding.inflate(inflater, container, false)
        return binding.root
    }

    private fun subscribeToData() {
        model.errorReplayRelay
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                context?.showToast("Error \n $it")
            }.subscribe()

        model.launchDetailsRelay
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                drawChart(it)
            }.subscribe()
    }

    private fun drawChart(launchesMap: FloatArray) {
        val adapter = MyAdapter()
        adapter.setData(launchesMap)

        binding.lineChart.setYAxisValueFormatter(defaultFormatter)
        binding.lineChart.setAdapter(adapter)
    }

    class MyAdapter : LineChartAdapter() {
        lateinit var yData: FloatArray
        override val count: Int
            get() = yData.size


        override fun getItem(index: Int): Any {
            return yData[index]
        }

        override fun getY(index: Int): Float {
            return yData[index]
        }

        internal fun setData(yData: FloatArray) {
            this.yData = yData
            notifyDataSetChanged()
        }
    }
}