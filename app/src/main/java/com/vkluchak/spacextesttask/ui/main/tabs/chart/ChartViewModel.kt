package com.vkluchak.spacextesttask.ui.main.tabs.chart

import androidx.lifecycle.ViewModel
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import com.vkluchak.spacextesttask.repository.LaunchRepository
import com.vkluchak.spacextesttask.vo.getMonthYearInt
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ChartViewModel @Inject constructor(private val launchRepository: LaunchRepository) :
    ViewModel() {
    val errorReplayRelay: PublishRelay<String> = PublishRelay.create()
    val launchDetailsRelay: BehaviorRelay<FloatArray> = BehaviorRelay.create()
    var isFirstInit = true

    fun getNumLaunchesForChart() {
        launchRepository.getAllLaunches()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError {
                errorReplayRelay.accept(it.message)
            }
            .doOnSuccess {
                val launchesMap = mutableMapOf<String, Int>()

                for (launch in it) {
                    val launchKey = getMonthYearInt(launch.launch_date_unix)
                    launchesMap.getOrPut(launchKey, { 0 })
                        .let { launchCunt -> launchesMap[launchKey] = launchCunt + 1 }
                }

                launchDetailsRelay.accept(launchesMap.values.map { it -> it.toFloat() }.toFloatArray())
            }
            .subscribe()
    }
}