package com.vkluchak.spacextesttask.ui.details

import androidx.lifecycle.ViewModel
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import com.vkluchak.spacextesttask.api.LaunchResponse
import com.vkluchak.spacextesttask.repository.LaunchRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DetailsViewModel : ViewModel {
    private val launchRepository: LaunchRepository

    var isFirstInit = true

    @Inject
    constructor(launchRepository: LaunchRepository) : super() {
        this.launchRepository = launchRepository
    }

    val errorReplayRelay: PublishRelay<String> = PublishRelay.create()
    val launchDetailsRelay: BehaviorRelay<LaunchResponse> = BehaviorRelay.create()

    fun getLaunchDetails(launchId: Int) {
        launchRepository.getLaunch(launchId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError {
                errorReplayRelay.accept(it.message)
            }
            .doOnSuccess { launchDetailsRelay.accept(it) }
            .subscribe()
    }
}