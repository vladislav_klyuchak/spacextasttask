package com.vkluchak.spacextesttask.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.ogaclejapan.smarttablayout.SmartTabLayout
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems
import com.vkluchak.spacextesttask.R
import com.vkluchak.spacextesttask.databinding.FragmentMainBinding
import com.vkluchak.spacextesttask.di.Injectable
import com.vkluchak.spacextesttask.ui.main.tabs.chart.ChartFragment
import com.vkluchak.spacextesttask.ui.main.tabs.list.LaunchListFragment
import javax.inject.Inject


class MainFragment : Fragment(), Injectable, SmartTabLayout.OnTabClickListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentMainBinding

    private lateinit var navController: NavController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        val pagerItems = FragmentPagerItems(context)
        pagerItems.add(FragmentPagerItem.of(getString(R.string.main_tab_name_list), LaunchListFragment::class.java))
        pagerItems.add(FragmentPagerItem.of(getString(R.string.main_tab_name_chart), ChartFragment::class.java))
        val tabAdapter = FragmentPagerItemAdapter(childFragmentManager, pagerItems)

        binding.vpContainer.adapter = tabAdapter


        binding.tabLayout.setViewPager(binding.vpContainer)
        binding.tabLayout.setOnTabClickListener(this)
    }
    override fun onTabClicked(position: Int) {

    }
}