package com.vkluchak.spacextesttask.ui.details

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxrelay2.ReplayRelay
import com.squareup.picasso.Picasso
import com.vkluchak.spacextesttask.R
import com.vkluchak.spacextesttask.databinding.ItemImageBinding

class LaunchImagesAdapter(private var imageList: MutableList<String>) :
    RecyclerView.Adapter<LaunchImagesAdapter.ImageViewHolder>() {

    private val onClickSubject = ReplayRelay.create<String?>()

    public fun getPositionClicks() = this.onClickSubject!!

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bind(onClickSubject, imageList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val binding: ItemImageBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_image, parent, false)
        return ImageViewHolder( binding)
    }

    fun setData(data: List<String>) {
        imageList.clear()
        imageList.addAll(data)
        notifyDataSetChanged()
    }

    override fun getItemCount() = imageList.size

    class ImageViewHolder(
        private val binding: ItemImageBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(onClickSubject : ReplayRelay<String?>, item: String?) {

            Picasso.get().load(item).into(binding.ivImage)
            binding.ibtnDownloadImg.setOnClickListener {
                onClickSubject.accept(item)
            }
            binding.executePendingBindings()
        }
    }

}