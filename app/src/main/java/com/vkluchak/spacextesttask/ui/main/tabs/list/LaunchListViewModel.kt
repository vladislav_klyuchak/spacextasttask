package com.vkluchak.spacextesttask.ui.main.tabs.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import androidx.paging.toLiveData
import com.vkluchak.spacextesttask.api.LaunchResponse
import com.vkluchak.spacextesttask.repository.LaunchRepository
import com.vkluchak.spacextesttask.repository.datasource.LaunchDataSourceFactory
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class LaunchListViewModel @Inject constructor(private val launchRepository: LaunchRepository) : ViewModel() {

    private val pageSize = 15
    private val compositeDisposable = CompositeDisposable()
    private val sourceFactory: LaunchDataSourceFactory
    var launchList: LiveData<PagedList<LaunchResponse>>
    var isFirstInit = true

    init {
        sourceFactory = LaunchDataSourceFactory(compositeDisposable, launchRepository)
        val config = PagedList.Config.Builder()
            .setPageSize(pageSize)
            .setInitialLoadSizeHint(pageSize * 2)
            .setEnablePlaceholders(false)
            .build()
        launchList = sourceFactory.toLiveData(config = config)
    }

    fun retry() {
        sourceFactory.launchDataSourceLiveData.value?.retry()
    }

    fun refresh() {
        sourceFactory.launchDataSourceLiveData.value?.invalidate()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}