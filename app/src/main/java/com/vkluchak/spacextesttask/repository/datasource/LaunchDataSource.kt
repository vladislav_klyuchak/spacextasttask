package com.vkluchak.spacextesttask.repository.datasource

import androidx.paging.PositionalDataSource
import com.vkluchak.spacextesttask.api.LaunchResponse
import com.vkluchak.spacextesttask.repository.LaunchRepository
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

/**
 * A data source that uses the before/after keys returned in page requests.
 * <p>
 * See ItemKeyedSubredditDataSource
 */
class PositionalLaunchDataSource(
    private val launchRepository: LaunchRepository,
    private val compositeDisposable: CompositeDisposable
) : PositionalDataSource<LaunchResponse>() {

    override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<LaunchResponse>) {
        compositeDisposable.add(
            launchRepository.getLaunches(0, params.requestedLoadSize)
                .subscribe({ launches ->
                    // clear retry since last request succeeded
                    setRetry(null)
                       callback.onResult(launches, 0)
                }, {
                    // keep a Completable for future retry
                    setRetry(Action { loadInitial(params, callback) })
                })
        )
    }

    override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<LaunchResponse>) {
        compositeDisposable.add(
            launchRepository.getLaunches(params.startPosition, params.loadSize)
                .subscribe({ it ->
                    // clear retry since last request succeeded
                    setRetry(null)
                    callback.onResult(it)
                }, {
                    // keep a Completable for future retry
                    setRetry(Action { loadRange(params, callback) })
                })
        )
   }

    /**
     * Keep Completable reference for the retry event
     */
    private var retryCompletable: Completable? = null


    fun retry() {
        retryCompletable?.let {
            compositeDisposable.add(
                retryCompletable!!
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ }, { throwable ->
                        Timber.e(throwable.message)
                    })
            )
        }
    }


    private fun setRetry(action: Action?) {
        if (action == null) {
            this.retryCompletable = null
        } else {
            this.retryCompletable = Completable.fromAction(action)
        }
    }
}