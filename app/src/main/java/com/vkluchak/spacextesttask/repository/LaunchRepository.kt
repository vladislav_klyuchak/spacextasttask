package com.vkluchak.spacextesttask.repository

import com.vkluchak.spacextesttask.api.LaunchResponse
import com.vkluchak.spacextesttask.api.LaunchApiService
import io.reactivex.Single


interface LaunchRepository {

    fun getLaunches(startPosition: Int, loadSize: Int): Single<List<LaunchResponse>>

    fun getLaunch(flightNumber: Int): Single<LaunchResponse>

    fun getAllLaunches(): Single<List<LaunchResponse>>
}

class LaunchRepositoryImpl(private val api: LaunchApiService) : LaunchRepository {

    override fun getAllLaunches() =
        api.getAllLaunches()
    /*  .map {
      val launchesMap = mutableMapOf<String, Int>()
      for (launch in it) {
          val launchKey = getMonthYearInt(launch.launch_date_unix)
          launchesMap.getOrPut(launchKey, { 1 })
              .let { launchCunt ->
                  launchesMap[launchKey] = launchCunt + 1
              }

      }
      launchesMap
  }
  */

    override fun getLaunch(flightNumber: Int) =
        api.getLaunch(flightNumber)

    override fun getLaunches(startPosition: Int, loadSize: Int) =
        api.getPagedLaunches(startPosition, loadSize)
}