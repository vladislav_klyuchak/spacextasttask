package com.vkluchak.spacextesttask.repository.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.vkluchak.spacextesttask.api.LaunchResponse
import com.vkluchak.spacextesttask.repository.LaunchRepository
import io.reactivex.disposables.CompositeDisposable

/**
 *
 * A simple data source factory which also provides a way to observe the last created data source.
 * This allows us to channel its network request status etc back to the UI. See the Listing creation
 * in the Repository class.
 */
class LaunchDataSourceFactory(
    private val compositeDisposable: CompositeDisposable,
    private val launchRepository: LaunchRepository
) : DataSource.Factory<Int, LaunchResponse>() {

    val launchDataSourceLiveData = MutableLiveData<PositionalLaunchDataSource>()

    override fun create(): DataSource<Int, LaunchResponse> {
        val usersDataSource = PositionalLaunchDataSource(launchRepository, compositeDisposable)
        launchDataSourceLiveData.postValue(usersDataSource)
        return usersDataSource
    }

}
