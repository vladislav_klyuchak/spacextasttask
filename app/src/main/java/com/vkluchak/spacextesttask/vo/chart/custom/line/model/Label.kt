package com.vkluchak.spacextesttask.vo.chart.custom.line.model

import android.graphics.RectF

class Label(val background: RectF, val textX: Float, val textY: Float, val text: String)