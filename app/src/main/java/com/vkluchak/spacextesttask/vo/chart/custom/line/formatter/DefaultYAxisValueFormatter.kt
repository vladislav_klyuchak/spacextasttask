package com.vkluchak.spacextesttask.vo.chart.custom.line.formatter

import android.graphics.RectF
import java.text.DecimalFormat
import java.text.NumberFormat

class DefaultYAxisValueFormatter {

    private val formatter: NumberFormat

    init {
        formatter = DecimalFormat("#.##")
    }

    fun getFormattedValue(value: Float, dataBounds: RectF, gridYDivisions: Int): String {
        return formatter.format(value.toDouble()).replace("^-(?=0(,0*)?$)".toRegex(), "") // Remove - sign if 0
    }
}
