package com.vkluchak.spacextesttask.vo.chart.custom.line

import android.graphics.RectF

/**
 * Helper class for handling scaling logic.
 */
class ScaleHelper(
    adapter: LineChartAdapter,
    drawingArea: RectF, numLabels: Int
) {

    // Width and height of the view
    private var width: Float
    private var height: Float
    // Min data value
    private var minY: Float = 0.toFloat()
    private var maxY: Float = 0.toFloat()
    private var minX: Float = 0.toFloat()
    private var maxX: Float = 0.toFloat()
    // Scale factor
    private var xScale: Double = 0.toDouble()
    private var yScale: Double = 0.toDouble()
    // Translation value
    private var xTranslation: Double = 0.toDouble()
    private var yTranslation: Double = 0.toDouble()

    init {
        // Get data
        width = drawingArea.width()
        height = drawingArea.height()
        val dataBounds = adapter.getDataBounds()
        // Calculate sizes
        calcMinMaxX(dataBounds)
        calcMinMaxY(dataBounds, numLabels, 0.05)
        calculateConversionFactors(drawingArea)
    }

    /**
     * Given a raw X value, it scales it to fit within our drawing area.
     */
    fun getX(rawX: Float): Float {
        return (rawX * xScale + xTranslation).toFloat()
    }

    /**
     * Given a raw Y value, it scales it to fit within our drawing area. This method also 'flips'
     * the value to be ready for drawing.
     */
    fun getY(rawY: Float): Float {
        return (height - rawY * yScale + yTranslation).toFloat()
    }

    /**
     * Given a scaled X value, it converts it back to the original raw value.
     */
    fun getRawX(scaledX: Float): Float {
        return ((scaledX - xTranslation) / xScale).toFloat()
    }

    /**
     * Given a scaled Y value, it converts it back to the original raw value.
     */
    fun getRawY(scaledY: Float): Float {
        return ((height + yTranslation - scaledY) / yScale).toFloat()
    }

    /**
     * Calculates minimum and maximum X values to represent.
     */
    private fun calcMinMaxX(bounds: RectF) {
        if (bounds.width() == 0f) {
            // If vertical line -> expand vertical bonds
            minX = bounds.left - 1
            maxX = bounds.right + 1
        } else {
            minX = bounds.left
            maxX = bounds.right
        }
    }

    /**
     * Calculates minimum and maximum Y values to represent.
     */
    private fun calcMinMaxY(bounds: RectF, numLabels: Int, coefficientExpansion: Double) {
        // Expand bonds
        var minYExpanded: Float
        var maxYExpanded: Float
        val halfLabels = numLabels / 2f
        if (bounds.height() == 0f) {
            // If it's a horizontal line, expand vertical bonds
            if (bounds.bottom == 0f) { // Horizontal 0 line -> expand just the top
                minYExpanded = 0f
                maxYExpanded = numLabels.toFloat()
            } else if (bounds.bottom > 0) { // Positive horizontal line -> expand both without going negative
                minYExpanded = bounds.top - halfLabels
                if (minYExpanded < 0) {
                    minYExpanded = 0f
                }
                maxYExpanded = bounds.bottom + halfLabels
            } else { // Negative horizontal line -> expand both without going positive
                minYExpanded = bounds.top - halfLabels
                maxYExpanded = bounds.bottom + halfLabels
                if (maxYExpanded > 0) {
                    maxYExpanded = 0f
                }
            }
        } else {
            // If it's not an horizontal line, use the coefficient of expansion
            minYExpanded = if (bounds.top == 0f) 0f else (bounds.top - bounds.height() * coefficientExpansion).toFloat()
            maxYExpanded = (bounds.bottom + bounds.height() * coefficientExpansion).toFloat()
        }
        val interval = Math.abs(maxYExpanded - minYExpanded)
        // Determine label granularity
        val granularity: Float
        if (interval >= 5) {
            granularity = 5f
        } else if (interval >= 2.5) {
            granularity = 1f
        } else {
            granularity = 0.5f
        }
        // Round min and max to the closest step
        val minYRounded = round(minYExpanded, granularity, false)
        val maxYRounded = round(maxYExpanded, granularity, true)
        val roundedInterval = Math.abs(maxYRounded - minYRounded)
        // Find the smallest interval that contains the data and it's divisible by the number of labels
        val divisibleInterval = lcm(roundedInterval, granularity)
        val step = (divisibleInterval / numLabels).toInt()
        val divisibleRoundedInterval: Float
        if (roundedInterval > 10) {
            divisibleRoundedInterval = round(step.toFloat(), granularity, true) * numLabels
        } else { // Allow smaller granularity for values smaller than 10
            divisibleRoundedInterval = step.toFloat() * numLabels
        }
        // Calculate how much we have to expand the original rounded interval
        val increment = Math.abs(divisibleRoundedInterval - roundedInterval)
        val expandMax: Float
        val expandMin: Float
        if (increment > granularity && minYRounded != 0f) {
            // If we can divide the increment in two parts we expand bottom and top
            expandMin = increment / 2f
            expandMax = expandMin
        } else {
            // Expand top
            expandMax = increment
            expandMin = 0f
        }
        // Calculate final min and max values
        minY = minYRounded - expandMin
        maxY = maxYRounded + expandMax
    }

    /**
     * Calculate the conversion factors to convert between raw and scaled values.
     */
    private fun calculateConversionFactors(drawingArea: RectF) {
        // Padding
        val leftPadding = drawingArea.left
        val topPadding = drawingArea.top
        // xScale will compress or expand the min and max x values to be just inside the view
        this.xScale = (width / (maxX - minX)).toDouble()
        // xTranslation will move the x points back between 0 - width
        this.xTranslation = leftPadding - minX * xScale
        // yScale will compress or expand the min and max y values to be just inside the view
        this.yScale = (height / (maxY - minY)).toDouble()
        // yTranslation will move the y points back between 0 - height
        this.yTranslation = minY * yScale + topPadding
    }
}
