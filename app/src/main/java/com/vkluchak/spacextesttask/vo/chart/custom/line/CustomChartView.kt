package com.vkluchak.spacextesttask.vo.chart.custom.line

import android.annotation.TargetApi
import android.content.Context
import android.database.DataSetObserver
import android.graphics.*
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.view.View
import android.view.ViewConfiguration
import com.vkluchak.spacextesttask.R
import com.vkluchak.spacextesttask.vo.chart.custom.line.formatter.DefaultYAxisValueFormatter
import com.vkluchak.spacextesttask.vo.chart.custom.line.model.Label
import com.vkluchak.spacextesttask.vo.chart.custom.touch.ScrubGestureDetector
import java.util.*


public class CustomChartView : View, ScrubGestureDetector.ScrubListener {

    private lateinit var render: CustomRenderDelegate
    // Touch
    private var scrubGestureDetector: ScrubGestureDetector? = null

    // Data
    private var adapter: LineChartAdapter? = null
    private lateinit var scaledXPoints: MutableList<Float> // Scaled x points
    private lateinit var scaledYPoints: MutableList<Float> // Scaled y points

    private var scaleHelper: ScaleHelper? = null

    // What to draw
    private val linePath = Path()
    private val pointPath = Path()
    private val gridLinePath = Path()
    private lateinit var gridLinesX: ArrayList<Float>
    private lateinit var gridLinesY: ArrayList<Float>

    private val drawingArea = RectF()
    private val scrubLinePath = Path()

    private var labelsY: MutableList<Label>? = null

    private var scrubCursorCurrentPos: PointF? = null
    private var scrubCursorTargetPos: PointF? = null
    private var scrubCursorCurrentLabel: Label? = null


    constructor(context: Context) : super(context) {
        init(
            context,
            null,
            R.attr.customChartViewStyle,
            R.style.CustomChartView
        )
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(
            context,
            attrs,
            R.attr.customChartViewStyle,
            R.style.CustomChartView
        )
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context, attrs, defStyleAttr, R.style.CustomChartView)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(context, attrs, defStyleAttr, defStyleRes)
    }


    /**
     * Initialises the view.
     */
    private fun init(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) {

        render = CustomRenderDelegate()

        render.processAttributes(context, attrs, defStyleAttr, defStyleRes)
        render.configPaintStyles()

        configGestureDetector(context)
        configDataStructure()
    }

    /**
     * Configures the gesture detector to listen to user touches.
     */
    private fun configGestureDetector(context: Context) {
        //   scrubAnimator = new ValueAnimator();
        scrubGestureDetector = ScrubGestureDetector(
            this,
            Handler(Looper.getMainLooper()),
            ViewConfiguration.get(context).scaledTouchSlop.toFloat()
        )

        setOnTouchListener(scrubGestureDetector)
    }

    /**
     * Configures the data structure where the chart data will be stored.
     */
    private fun configDataStructure() {
        scaledXPoints = ArrayList<Float>()
        scaledYPoints = ArrayList<Float>()
    }


    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        // Called when the view is first assigned a size, and again if the size changes for any reason
        // All calculations related to positions, dimensions, and any other values must be done here (not in onDraw)
        super.onSizeChanged(w, h, oldw, oldh)
        calculateDrawingArea()
        populatePaths()
    }

    override fun onDraw(canvas: Canvas) {
        // No allocations here, Allocate objects during initialization, or between animations. Never make an allocation while an animation is running.
        // In addition to making onDraw() leaner, also make sure it's called as infrequently as possible. Most calls to onDraw() are the result of a call to invalidate(), so eliminate unnecessary calls to invalidate().
        super.onDraw(canvas)
        canvas.drawPath(gridLinePath, render.gridLinePaint)

        canvas.drawPath(linePath, render.linePaint)
        canvas.drawPath(pointPath, render.pointPaint)
        drawScrubCursor(canvas)
        drawLabels(canvas)

    }

    /**
     * Draws chart labels in the canvas.
     */
    private fun drawLabels(canvas: Canvas) {
        for (label in labelsY.orEmpty()) {
            canvas.drawRoundRect(
                label.background,
                render.labelBackgroundRadius,
                render.labelBackgroundRadius,
                render.labelBackgroundPaint
            )
            canvas.drawText(label.text, label.textX, label.textY, render.labelTextPaint)
        }
    }

    /**
     * Draws scrub cursor.
     */
    private fun drawScrubCursor(canvas: Canvas) {
        scrubCursorCurrentLabel?.let {
            canvas.drawRoundRect(
                it.background,
                render.labelBackgroundRadius,
                render.labelBackgroundRadius,
                render.labelBackgroundPaint
            )
            canvas.drawText(
                it.text,
                it.textX,
                it.textY,
                render.labelTextPaint
            )
        }
    }

    /**
     * Calculates the area where we can draw (essentially the bounding rect minus any padding).
     * The area is represented in a rectangle.
     */
    private fun calculateDrawingArea() {
        drawingArea.set(
            paddingStart.toFloat(), paddingTop.toFloat(),
            (width - paddingEnd).toFloat(),
            (height - paddingBottom).toFloat()
        )
    }

    /**
     * Populates the path to draw.
     */
    private fun populatePaths() {
        if (adapter == null || width == 0 || height == 0) {
            return
        } else {
            val numPoints = adapter!!.count
            // To draw anything, we need 2 or more points
            if (numPoints < 2) {
                clearData()
                return
            }
            scaleHelper = ScaleHelper(adapter!!, drawingArea, render.gridYDivisions)
            // Check formatter
            render.yAxisValueFormatter?.let { DefaultYAxisValueFormatter() }
            // Populate paths
            populateGrid()
            populateLine(numPoints)

            populateLabels()
            resetScrubCursor()
            invalidate()
        }
    }

    /**
     * Populates grid path.
     */
    fun populateGrid() {
        gridLinePath.reset()
        val gridLeft = drawingArea.left
        val gridBottom = drawingArea.bottom
        val gridTop = drawingArea.top
        val gridRight = drawingArea.right
        var gridLineSpacing: Float
        // Grid X axis
        gridLinesX = ArrayList(render.gridXDivisions)
        gridLineSpacing = (gridRight - gridLeft) / render.gridXDivisions
        for (i in 0 until render.gridXDivisions) {
            gridLinesX.add(gridLeft + i * gridLineSpacing)
            gridLinePath.moveTo(gridLinesX[i], gridTop)
            gridLinePath.lineTo(gridLinesX[i], gridBottom)
        }
        // Grid Y axis
        gridLinesY = ArrayList(render.gridYDivisions)
        gridLineSpacing = (gridBottom - gridTop) / render.gridYDivisions
        for (i in 0 until render.gridYDivisions) {
            gridLinesY.add(gridTop + i * gridLineSpacing)
            gridLinePath.moveTo(gridLeft, gridLinesY[i])
            gridLinePath.lineTo(gridRight, gridLinesY[i])
        }
    }

    /**
     * Populates line of chart.
     */
    fun populateLine(numPoints: Int) {
        linePath.reset()
        pointPath.reset()
        scaledXPoints.clear()
        scaledYPoints.clear()
        scaleHelper?.let {
            for (i in 0 until numPoints) {
                // Scale points relative to the drawing area
                val x = it.getX(adapter!!.getX(i))
                val y = it.getY(adapter!!.getY(i))
                scaledXPoints.add(x)
                scaledYPoints.add(y)
                pointPath.addCircle(x, y, 10f, Path.Direction.CW)
                // Populate line path
                if (i == 0) {
                    linePath.moveTo(x, y)
                } else {
                    linePath.lineTo(x, y)
                }
            }
        }
    }

    /**
     * Populates labels.
     */
    private fun populateLabels() {
        // Populate labels
        labelsY = ArrayList<Label>(gridLinesY.size - 1)
        var labelText: String
        val labelTextBounds = Rect()
        var labelTextX: Float
        var labelTextY: Float
        var background: RectF
        val bgLeft = drawingArea.left + render.labelMargin
        var bgTop: Float
        var bgRight: Float
        var bgBottom: Float
        for (i in 1 until gridLinesY.size) { // No label in the last grid line
            // Format text
            labelText = render.yAxisValueFormatter!!.getFormattedValue(
                scaleHelper!!.getRawY(gridLinesY[i]),
                adapter!!.getDataBounds(),
                render.gridYDivisions
            )
            // Calculate background
            render.labelTextPaint.getTextBounds(labelText, 0, labelText.length, labelTextBounds)
            bgTop = gridLinesY[i] - labelTextBounds.height() / 2f - render.labelBackgroundPaddingVertical
            bgRight =
                bgLeft + labelTextBounds.width().toFloat() + render.labelBackgroundPaddingHorizontal * 2
            bgBottom =
                bgTop + labelTextBounds.height().toFloat() + render.labelBackgroundPaddingVertical * 2
            background = RectF(bgLeft, bgTop, bgRight, bgBottom)
            // Calculate text position
            labelTextX = bgLeft + render.labelBackgroundPaddingHorizontal - labelTextBounds.left
            labelTextY =
                bgBottom - render.labelBackgroundPaddingVertical - labelTextBounds.bottom.toFloat()
            labelsY!!.add(Label(background, labelTextX, labelTextY, labelText))
        }
    }

    /**
     * Populates scrub cursor.
     */
    private fun populateScrubCursor(newScrubCursorTargetPos: PointF) {

        val labelText = render.yAxisValueFormatter!!.getFormattedValue(
            scaleHelper!!.getRawY(newScrubCursorTargetPos.y),
            adapter!!.getDataBounds(),
            render.gridYDivisions
        )
        val labelTextBounds = Rect()
        // Calculate background
        render.labelTextPaint.getTextBounds(labelText, 0, labelText.length, labelTextBounds)

        val bgLeft = newScrubCursorTargetPos.x - (labelTextBounds.width() * 2f)
        val bgRight = newScrubCursorTargetPos.x + (labelTextBounds.width().toFloat() * 2)

        val bgTop =
            newScrubCursorTargetPos.y - (labelTextBounds.height().toFloat() * 2) - render.labelBackgroundPaddingVertical
        val bgBottom = bgTop + labelTextBounds.height().toFloat() + render.labelBackgroundPaddingVertical * 2

        val background = RectF(bgLeft, bgTop, bgRight, bgBottom)

        // Calculate text position
        val labelTextX =
            newScrubCursorTargetPos.x - render.labelBackgroundPaddingHorizontal + (labelTextBounds.width() / 2)
        val labelTextY = bgBottom - render.labelBackgroundPaddingVertical - labelTextBounds.bottom.toFloat()

        scrubCursorCurrentLabel = Label(background, labelTextX, labelTextY, labelText)

        invalidate()
    }

    private fun resetScrubCursor() {
        handler.removeCallbacksAndMessages(null)
        scrubCursorTargetPos = null
        scrubCursorCurrentPos = scrubCursorTargetPos
    }

    private val dataSetObserver = object : DataSetObserver() {
        override fun onChanged() {
            super.onChanged()
            populatePaths()
        }

        override fun onInvalidated() {
            super.onInvalidated()
            clearData()
        }
    }

    fun setYAxisValueFormatter(yAxisValueFormatter: DefaultYAxisValueFormatter?) {
        render.yAxisValueFormatter = yAxisValueFormatter ?: DefaultYAxisValueFormatter()
    }

    override fun onScrubbed(x: Float, y: Float) {
        if (adapter == null || adapter!!.count == 0)
            return
        parent.requestDisallowInterceptTouchEvent(true)
        if (scrubCursorTargetPos == null) {
            handler.removeCallbacksAndMessages(null)
        }
        val index = getNearestIndex(scaledXPoints, x)
        populateScrubCursor(
            PointF(
                scaleHelper!!.getX(adapter!!.getX(index)),
                scaleHelper!!.getY(adapter!!.getY(index))
            )
        )
    }

    override fun onScrubEnded() {
        scrubLinePath.reset()
        scrubCursorCurrentPos = scrubCursorTargetPos
        scrubCursorTargetPos = null
        handler.postDelayed({ this.hideCursor() }, 3000)
    }

    private fun hideCursor() {
        scrubCursorCurrentPos = null
        invalidate()
    }

    /**
     * Clears data to draw.
     */
    private fun clearData() {
        scaleHelper = null
        linePath.reset()
        pointPath.reset()
        gridLinePath.reset()
        invalidate()
    }


    fun setAdapter(adapter: LineChartAdapter?) {
        if (this.adapter != null) {
            this.adapter!!.unregisterDataSetObserver(dataSetObserver)
        }
        this.adapter = adapter
        if (this.adapter != null) {
            this.adapter!!.registerDataSetObserver(dataSetObserver)
        }
        populatePaths()
    }

}