package com.vkluchak.spacextesttask.vo.chart.custom.line

import java.util.*

/**
 * Returns the nearest index into the list of data for the given x coordinate.
 */
fun getNearestIndex(points: List<Float>, x: Float): Int {
    var index = Collections.binarySearch(points, x)

    // if binary search returns positive, we had an exact match, return that index
    if (index >= 0) return index

    // otherwise, calculate the binary search's specified insertion index
    index = -1 - index

    // if we're inserting at 0, then our guaranteed nearest index is 0
    if (index == 0) return index

    // if we're inserting at the very end, then our guaranteed nearest index is the final one
    if (index == points.size) return --index

    // otherwise we need to check which of our two neighbors we're closer to
    val deltaUp = points[index] - x
    val deltaDown = x - points[index - 1]
    if (deltaUp > deltaDown) {
        // if the below neighbor is closer, decrement our index
        index--
    }

    return index
}

/**
 * Rounds number to the nearest multiple of the given multipleOf.
 *
 * @param number     number to round.
 * @param multipleOf multiple used in the rounding.
 * @param roundUp    if true ceil rounding if false floor rounding.
 */
fun round(number: Float, multipleOf: Float, roundUp: Boolean): Float {
    return if (roundUp) {
        (Math.ceil((number / multipleOf).toDouble()) * multipleOf).toFloat()
    } else {
        Math.floor((number / multipleOf).toDouble()).toFloat() * multipleOf
    }
}

/**
 * Least common multiple (LCM).
 */
fun lcm(a: Float, b: Float): Float {
    return a * (b / gcd(a, b))
}

/**
 * Greatest Common Divisor (GCD).
 */
fun gcd(a: Float, b: Float): Float {
    var a = a
    var b = b
    while (b > 0) {
        val temp = b
        b = a % b // % is remainder
        a = temp
    }
    return a
}