package com.vkluchak.spacextesttask.vo.chart.custom.line

import android.content.Context
import android.graphics.Paint
import android.text.TextPaint
import android.util.AttributeSet
import androidx.annotation.ColorInt
import com.vkluchak.spacextesttask.R
import com.vkluchak.spacextesttask.vo.chart.custom.line.formatter.DefaultYAxisValueFormatter

class CustomRenderDelegate {

    @ColorInt
    var lineColor: Int = 0
    var lineWidth: Float = 0.toFloat()

    @ColorInt
    var gridLineColor: Int = 0
    var gridLineWidth: Float = 0.toFloat()
    var gridXDivisions: Int = 0
    var gridYDivisions: Int = 0

    @ColorInt
    var baseLineColor: Int = 0
    var baseLineWidth: Float = 0.toFloat()

    var scrubEnabled: Boolean = false
    @ColorInt
    var scrubLineColor: Int = 0
    var scrubLineWidth: Float = 0.toFloat()

    var labelMargin: Float = 0.toFloat()
    @ColorInt
    var labelTextColor: Int = 0
    var labelTextSize: Float = 0.toFloat()
    @ColorInt
    var labelBackgroundColor: Int = 0
    var labelBackgroundRadius: Float = 0.toFloat()
    var labelBackgroundPaddingHorizontal: Float = 0.toFloat()
    var labelBackgroundPaddingVertical: Float = 0.toFloat()


    // How to draw
    val linePaint = Paint(Paint.ANTI_ALIAS_FLAG)
    val pointPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    val gridLinePaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val baseLinePaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val scrubLinePaint = Paint(Paint.ANTI_ALIAS_FLAG)
    val labelTextPaint = TextPaint(Paint.ANTI_ALIAS_FLAG)
    private var labelTextOriginalAlpha: Int = 0
    val labelBackgroundPaint = TextPaint(Paint.ANTI_ALIAS_FLAG)
    private var labelBackgroundOriginalAlpha: Int = 0
    var yAxisValueFormatter: DefaultYAxisValueFormatter? = null


    /**
     * Reads the xml attributes and configures the view based on them.
     */
    fun processAttributes(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.CustomChartView, defStyleAttr, defStyleRes)
        try {
            lineColor = a.getColor(R.styleable.CustomChartView_chartview_lineColor, 0)
            lineWidth = a.getDimension(R.styleable.CustomChartView_lineWidth, 0f)

            gridLineColor = a.getColor(R.styleable.CustomChartView_gridLineColor, 0)
            gridLineWidth = a.getDimension(R.styleable.CustomChartView_gridLineWidth, 0f)
            gridXDivisions = a.getInteger(R.styleable.CustomChartView_gridXDivisions, 0)
            gridYDivisions = a.getInteger(R.styleable.CustomChartView_gridYDivisions, 0)

            baseLineColor = a.getColor(R.styleable.CustomChartView_baseLineColor, 0)
            baseLineWidth = a.getDimension(R.styleable.CustomChartView_baseLineWidth, 0f)

            scrubEnabled = a.getBoolean(R.styleable.CustomChartView_scrubEnabled, true)
            scrubLineColor = a.getColor(R.styleable.CustomChartView_scrubLineColor, 0)
            scrubLineWidth = a.getDimension(R.styleable.CustomChartView_scrubLineWidth, 0f)

            labelMargin = a.getDimension(R.styleable.CustomChartView_labelMargin, 0f)
            labelTextColor = a.getColor(R.styleable.CustomChartView_labelTextColor, 0)
            labelTextSize = a.getDimension(R.styleable.CustomChartView_labelTextSize, 0f)
            labelBackgroundColor = a.getColor(R.styleable.CustomChartView_labelBackgroundColor, 0)
            labelBackgroundRadius = a.getDimension(R.styleable.CustomChartView_labelBackgroundRadius, 0f)
            labelBackgroundPaddingHorizontal =
                a.getDimension(R.styleable.CustomChartView_labelBackgroundPaddingHorizontal, 0f)
            labelBackgroundPaddingVertical =
                a.getDimension(R.styleable.CustomChartView_labelBackgroundPaddingVertical, 0f)
        } finally {
            a.recycle()
        }
    }

    /**
     * Configures how the different elements of the chart have to be painted.
     */
    fun configPaintStyles() {
        // Line
        linePaint.style = Paint.Style.STROKE
        linePaint.color = lineColor
        linePaint.strokeWidth = lineWidth
        linePaint.strokeCap = Paint.Cap.ROUND

        // Point
        pointPaint.style = Paint.Style.FILL
        pointPaint.color = lineColor

        // Grid
        gridLinePaint.style = Paint.Style.STROKE
        gridLinePaint.color = gridLineColor
        gridLinePaint.strokeWidth = gridLineWidth
        // Base line
        baseLinePaint.style = Paint.Style.STROKE
        baseLinePaint.color = baseLineColor
        baseLinePaint.strokeWidth = baseLineWidth
        // Scrub line
        scrubLinePaint.style = Paint.Style.STROKE
        scrubLinePaint.strokeWidth = scrubLineWidth
        scrubLinePaint.color = scrubLineColor
        scrubLinePaint.strokeCap = Paint.Cap.ROUND
        // Labels
        labelTextPaint.color = labelTextColor
        labelTextPaint.textSize = labelTextSize
        labelTextPaint.textAlign = Paint.Align.LEFT
        labelTextOriginalAlpha = labelTextPaint.alpha
        labelBackgroundPaint.color = labelBackgroundColor
        labelBackgroundPaint.style = Paint.Style.FILL
        labelBackgroundOriginalAlpha = labelBackgroundPaint.alpha
    }

}