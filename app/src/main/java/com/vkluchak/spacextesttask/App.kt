package com.vkluchak.spacextesttask

import android.app.Activity
import android.app.Application
import com.facebook.stetho.Stetho
import com.squareup.picasso.Picasso
import com.vkluchak.spacextesttask.di.AppInjector
import com.vkluchak.spacextesttask.di.RxConfigModule
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import timber.log.Timber
import javax.inject.Inject

class App : Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        initLogger()
        Stetho.initializeWithDefaults(this)

        AppInjector.init(this)

        RxConfigModule().configure()
    }

    private fun initLogger() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            Picasso.get().isLoggingEnabled = true
        }
    }

    override fun activityInjector() = dispatchingAndroidInjector
}